const fs = require('fs');
const querystring = require('querystring');
/*
Nota: Los readFileSync y el writeFileSync deberían ser cambiados a readFile y writeFile
*/

function soundex(s) { //copiado de internet, funcionamiento desconocido
    var a = s.toLowerCase().split(''),
        f = a.shift(),
        r = '',
        codes = {
            a: '', e: '', i: '', o: '', u: '',
            b: 1, f: 1, p: 1, v: 1,
            c: 2, g: 2, j: 2, k: 2, q: 2, s: 2, x: 2, z: 2,
            d: 3, t: 3,
            l: 4,
            m: 5, n: 5,
            r: 6
        };
    r = f +
        a
        .map(function (v, i, a) { return codes[v] })
        .filter(function (v, i, a) {
            return ((i === 0) ? v !== codes[f] : v !== a[i - 1]);
        })
        .join('');
    return (r + '000').slice(0, 4).toUpperCase();
}

function comp(str1,str2){                   // compara los dos nombres
    stra = str1.toLowerCase().split(" ");   // si tiene varias palabras
    strb = str2.toLowerCase().split(" ");
    str1 = soundex(str1);   //soundex
    str2 = soundex(str2);
    let palcom = 0;  //palabras en comun
    let letcom = 0;  //letras en comun
    //console.log("SOUNDEX: ",str1," ",str2); 

    for (let i = 0; i<=str1.length;i++){ //compara los soundex mas o menos
        if(str2.includes(str1[i])){
            letcom +=1;
        } 
    }
    for (let j = 0; j <= stra.length ;j++){ // busca palabras en comun
        if(strb.includes(stra[j])){
            palcom +=1;
            console.log("palabra en comun");
        } 
    }

    if((str1 == str2)||(palcom > 0)){ // si tienen palabras en comun o los soundex son iguales 
        return "OK";
    } else if (letcom > 2){ // si los soundex tienen algo en comun
        return "ok?";
    } else {     //sino
        return "?"
    }
}

function relacionar(dat){ 
    //dat contiene dos infos (id1, id2), y el valor de c/u es id1_ciudad1

    var datos = fs.readFileSync("./datos.json",{"Content-Type":"application/JSON"}); //lee el archivo JSON

    datos = JSON.parse(datos); //convierte todos los datos a un objeto
    dat = JSON.parse(dat); //convierte todos los dos datos de las ciudades a relacionar

    let spl1 = dat["id1"].split("_",2); //los separa en "id" (indice 0) y "nombre" (indice 1)
    let spl2 = dat["id2"].split("_",2); //porque los datos llegan como 1234_ciudad

    //rearma los datos asi son mas usables
    dat["id1"] = spl1[0]    
    dat["ciudad_1"] = spl1[1]      
    dat["id2"] = spl2[0]        
    dat["ciudad_2"] = spl2[1]       
    dat["check"] = comp(dat["ciudad_1"],dat["ciudad_2"]);

    //console.log(dat);

    let existente1 = datos["relcids"].find(cda => cda["id1"] === dat["id1"]);   //si la relacion ya existe
    let existente2 = datos["relcids"].find(cda => cda["id2"] === dat["id2"]);   //la guarda en estos objs
    if (existente1 && existente2){          //si la relacion existe doblemente
        Object.assign(existente2, dat);     //las hace iguales            |
        Object.assign(existente1, dat);     //y las elimina con el filter v
        if(existente1 !== existente2){      //esto no deberia funcionar pero arreglo el error?
            datos["relcids"] = datos["relcids"].filter(relac => relac !== existente1); 
        }
    } else if (existente1){             
        Object.assign(existente1, dat); //le cambia la relacion
    } else if(existente2){
        Object.assign(existente2, dat); 
    } else{
        datos["relcids"].push(dat); //los agrega al objeto con las relaciones
    }
    
    datos = JSON.stringify(datos, null, 4); //lo convierte de nuevo a JSON
    fs.writeFileSync("./datos.json",datos); //lo escribe en el archivo 
}


function delAall(){ //esta funcion elimina todos los datos de las relaciones (para testing)
    var datos = fs.readFileSync("./datos.json",{"Content-Type":"application/JSON"});
    datos = JSON.parse(datos);
    datos["relcids"] = [{
        "id1": "",
        "id2": "",
        "Ciudad 1":"",
        "Ciudad 2":"",  
        "check":""  }]; //necesita tener estos datos o sino se rompe la pag
    datos = JSON.stringify(datos, null, 4); 
    fs.writeFileSync("./datos.json",datos); 
}

//exports
exports.relacionar = relacionar;
exports.del = delAall;


