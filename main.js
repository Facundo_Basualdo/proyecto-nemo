const http = require('http');
const fs = require('fs');
const url = require('url');
const querystring = require('querystring');
const map = require("./map.js");

const server = http.createServer(function(req,res){
    console.log(req.url);
    switch(req.url){
    case "/" : 
        fs.readFile('./pagina/index.html', function(err,data){
            if(err){throw err}
            res.writeHead(200,{"Content-Type":"text/html"});
            res.write(data);
            res.end();
        });
    break;
    case "/datos" : 
        fs.readFile('./datos.json', function(err,data){
            if(err){throw err}
            res.writeHead(200,{"Content-Type":"application/JSON"});
            res.write(data);
            res.end();
        });
    break;
    case "/style" :
        fs.readFile('./pagina/style.css', function(err,data){
            if(err){throw err}
            res.writeHead(200,{"Content-Type":"text/css"});
            res.write(data);
            res.end();
        });
    break;
    case "/script" :
        fs.readFile('./pagina/script.js', function(err,data){
            if(err){throw err}
            res.writeHead(200,{"Content-Type":"text/javascript"});
            res.write(data);
            res.end();
        });
    break;
    case "/map" :
        var dataString = '';
        req.on('data', function (data, err){
            if(err){throw err}
            dataString += data
        })
        .on('end', function (){
            var dataObject = querystring.parse(dataString),
            dataJSON = JSON.stringify(dataObject);
            console.log(dataJSON);
            map.relacionar(dataJSON);
            res.writeHead(302,{
                "Location":"/"
            });
            res.end();
        });
    break;
    case "/delete" :
        map.del();
        res.writeHead(302,{"Location":"/",});
        res.end();
    break;
    default: 
        console.log("no encontrado");
        res.writeHead(404,{"Content-Type":"text/plain"});
        res.end("404 no encontrado");
    }
});
console.log("servidor iniciado");
server.listen(3000);
