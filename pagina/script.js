//obtiene los datos del archivo json
var req = new XMLHttpRequest();
var url = "/datos";
var datos;
//Tablas
var tab1 = document.getElementById("tac1");
var tab2 = document.getElementById("tac2");
var tabr = document.getElementById("tar");
var desp1 = document.getElementById("ciud1");
var desp2 = document.getElementById("ciud2");
var desp3 = document.getElementById("elim");
var pais1 = document.getElementById("pa1");
var pais2 = document.getElementById("pa2"); 
var tabPais1 = document.getElementById("tacPais1");
var tabPais2 = document.getElementById("tacPais2");
var despa1 = document.getElementById("despais1");
var despa2 = document.getElementById("despais2");
var cuaPais = document.getElementById("divPais");
var cuaCiuds = document.getElementById("tabsCiuds");

req.overrideMimeType("application/json");
req.open('GET', url, true);
req.onload  = function() {
datos = JSON.parse(req.responseText);
if(cuaPais.style.display=="none"){
    cuaPais.style.display="inline-block";
    genTabPais(tabPais1, "cids1");
    genTabPais(tabPais2, "cids2");
    genDespPais(despa1, "cids1");
    genDespPais(despa2, "cids2");
}
else{
    genTab(tab1,"cids1");
    genTab(tab2,"cids2");
    genTab(tabr,"relcids");
    genDesp(desp1,"cids1");
    genDesp(desp2,"cids2");
    genDespPaisCiuds(pais1,"cids1");
    genDespPaisCiuds(pais2,"cids2");
    }
};
req.send(null);

function genTab(tab,dat){
    genThead(tab,dat);
    genTbody(tab,dat);
}
function genTabPais(tab, dat){
    genTHeadPais(tab, dat);
    genTbodyPais(tab, dat);
}

function genTHeadPais(tab, dat){
    let thead = document.createElement("thead");
    tab.appendChild(thead);
    let trow0 = document.createElement("tr");
    thead.appendChild(trow0);
    let cell0 = document.createElement("th");
    trow0.appendChild(cell0);
    trow0.style.columnSpan="2";
    trow0.style.textAlign="center";
    cell0.colSpan="10";
    cell0.innerHTML = "Paises";
    if (dat=="cids1"){
        cell0.innerHTML = "Paises 1";
    }else if(dat=="cids2"){
        cell0.innerHTML = "Paises 2";
    }
    let trow = document.createElement("tr");
    thead.appendChild(trow);
    for(let key of Object.keys(datos[dat][0])){
        let cell = document.createElement("th");
        cell.innerHTML = key;
        trow.appendChild(cell);
    }
}
function genTbodyPais(tab, dat){
    let tbody = document.createElement("tbody");
    tab.appendChild(tbody);
    for(let ent of Object.entries(datos[dat])){
        let trow = document.createElement("tr");

        let desp;
        if(tab === tabPais1){
            desp = despa1;
        }else if(tab===tabPais2){
            desp =  despa2;
        }else {
            desp = desp3; //no hace nada
        }                                              
        trow.addEventListener("click",function(){       
            for(el of this.parentElement.children){     
                el.style.backgroundColor = "#d8ddee";
                el.style.border="";
            }                                           
            this.style.backgroundColor = "#a9b3d3";
            this.style.border = "2px solid #6a64be";
            desp.value = this.children[0].innerHTML + "_" + this.children[1].innerHTML;    
        });
        tbody.appendChild(trow);
        for(let val in ent[1]){
            let td = document.createElement("td")
            td.innerHTML = ent[1][val];
            
            trow.appendChild(td);
        }
    }
}

function genDespPais(desp, dat){
    for(let ent of Object.entries(datos[dat])){
        let opt = document.createElement("option");
        opt.innerHTML = ent[1]["pais"];
        opt.value = ent[1]["pais"];
        desp.appendChild(opt);
    }
}

function genThead(tab,dat){
    let thead = document.createElement("thead");
    tab.appendChild(thead);
    let trow0 = document.createElement("tr");
    thead.appendChild(trow0);
    let cell0 = document.createElement("th");
    trow0.appendChild(cell0);
    trow0.style.columnSpan="2";
    trow0.style.textAlign="center";
    cell0.colSpan="10";
    
    if (dat=="cids1"){
        cell0.innerHTML = "Ciudades 1";
    }else if(dat=="cids2"){
        cell0.innerHTML = "Ciudades 2";
    }else{
        cell0.innerHTML = "Ciudades relacionadas";
    }
    
    let trow = document.createElement("tr");
    thead.appendChild(trow);
    for(let key of Object.keys(datos[dat][0])){
        let cell = document.createElement("th");
        cell.innerHTML = key;
        trow.appendChild(cell);
    }
}

function genTbody(tab,dat){
    let tbody = document.createElement("tbody");
    tab.appendChild(tbody);
    for(let ent of Object.entries(datos[dat])){
        let trow = document.createElement("tr");

        let desp;
        if(tab === tab1){
            desp = desp1;
        }else if(tab===tab2){
            desp =  desp2;
        }else {
            desp = desp3; //no hace nada
        }                                              
        trow.addEventListener("click",function(){       
            for(el of this.parentElement.children){     
                el.style.backgroundColor = "#d8ddee";
                el.style.border="";
            }                                           
            this.style.backgroundColor = "#a9b3d3";
            this.style.border = "2px solid #6a64be";
            desp.value = this.children[0].innerHTML + "_" + this.children[1].innerHTML;    
        });
        tbody.appendChild(trow);
        for(let val in ent[1]){
            let td = document.createElement("td")
            td.innerHTML = ent[1][val];
            switch(td.innerHTML){ //colores
                case "OK":
                    td.style.minWidth = "40px";
                    td.style.backgroundColor = "#57f281";
                break;
                case "ok?":
                    td.style.minWidth = "40px";
                    td.style.backgroundColor = "#e2f257";
                break;
                case "ok??":
                    td.style.minWidth = "40px";
                    td.style.backgroundColor = "#e2f257";
                break;
                case "?":
                    td.style.minWidth = "40px";
                    td.style.backgroundColor = "#f5563d";
                break;
            }
            trow.appendChild(td);
        }
    }
}

function genDesp(desp, dat){
    
    for(let ent of Object.entries(datos[dat])){
            let opt = document.createElement("option");
            desp.appendChild(opt);
            opt.innerHTML = ent[1]["nombre"];
            opt.value = ent[1]["id"] + "_" + ent[1]["nombre"];
        }
    }

function genDespPaisCiuds(desp, dat){
    for(let ent of Object.entries(datos[dat])){
        let opt = document.createElement("option");
        opt.innerHTML = ent[1]["pais"];
        opt.value = ent[1]["pais"];
        desp.appendChild(opt);
    }
    desp.addEventListener("change",function(){
        let tab;
        if(dat == "cids1"){
            tab = tab1;
        } else if (dat == "cids2"){
            tab = tab2;
        }
        let tr = tab.getElementsByTagName("tr");
        this.value.toLowerCase();
        for (let i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                let txtValue = td.textContent || td.innerText;
                if (txtValue.toLowerCase().indexOf(this.value.toLowerCase()) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }   
    });
}

function filtrar(){
    cuaPais.style.display="none";
    cuaCiuds.style.display="inline-block"; 
}
